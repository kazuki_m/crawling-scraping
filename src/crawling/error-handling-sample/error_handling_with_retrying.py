import time
import requests
from retrying import retry


# 一時的なエラーを表すステータスコード
TEMPORARY_ERROR_CODES = (408, 500, 502, 503, 504)


def main():
    response = fetch('http://httpbin.org/status/200, 404, 503')

    if 200 <= response.status_code < 300:
        print('SUCCESS!')
    else:
        print('Error!')

# stop_max_attempt_number 最大リトライ回数
# wait_exponential_multiplier 指数関数的なウェイトを取る場合の、初回のウェイトをミリ秒単位で指定する。


@retry(stop_max_attempt_number=3, wait_exponential_multiplier=1000)
# 指定したURLを取得してResponseオブジェクトを返す。
def fetch(url):

    print('Retriving {0}'.format(url))
    response = requests.get(url)
    print('Status is {0}'.format(response.status_code))

    if response.status_code not in TEMPORARY_ERROR_CODES:
        return response

    raise Exception('Temporary Error {0}'.format(response.status_code))


if __name__ == '__main__':
    main()
