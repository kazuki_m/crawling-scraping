import time
import requests


# 一時的なエラーを表すステータスコード
TEMPORARY_ERROR_CODES = (408, 500, 502, 503, 504)


def main():
    response = fetch('http://httpbin.org/status/200, 404, 503')

    if 200 <= response.status_code < 300:
        print('SUCCESS!')
    else:
        print('Error!')


def fetch(url):
    '''
    指定したURLを取得してResponseオブジェクトを返す。
    一時的なエラーが起きた場合は最大３回リトライする
    '''

    max_retries = 3
    current_retries = 0

    while True:
        try:
            print('Retriving {0}'.format(url))
            response = requests.get(url)
            print('Status is {0}'.format(response.status_code))

            if response.status_code not in TEMPORARY_ERROR_CODES:
                return response

        except requests.exceptions.RequestException as ex:
            # ネットワークレベルのエラーはリトライする
            print('Exceptin occured {0}'.format(ex))

            current_retries += 1
            if current_retries >= max_retries:
                raise Exception('Too many retries')
            # リトライ間隔を求める
            wait_seconds = 2**(current_retries - 1)
            print('Waiting {0} seconds...'.format(wait_seconds))
            time.sleep(wait_seconds)


if __name__ == '__main__':
    main()
