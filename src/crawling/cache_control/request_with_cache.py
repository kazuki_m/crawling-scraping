import requests
from cachecontrol import CacheControl


session = requests.session()
cached_session = CacheControl(session)

# 初回、False
response = cached_session.get('https://docs.python.org/3/')
print(response.from_cache)

# ２回目の実行、EtagとLast-Modifiedを値を使用して更新されているか確認する。
response = cached_session.get('https://docs.python.org/3/')
print(response.from_cache)