from pymongo import MongoClient


client = MongoClient('localhost', 27017)

# テストデータベースを取得、存在しない場合は作成
db = client.test
db = client['test']

collection = db.sports
collection = db['sports']

collection.insert_one(
    {
        'name': '東京スカイツリー',
        'prefecture': '東京'
    }
)

collection.insert_many(
    [
        {
            'name': '東京ディズニーランド',
            'prefecture': '千葉'
        },
        {
            'name': '東京ドーム',
            'prefecture': '東京'
        }
    ]
)

collection.find()

for spot in collection.find():
    print(spot)

for spot in collection.find({'prefecture': '東京'}):
    print(spot)

collection.find_one()
