# -*- coding: utf-8 -*-

#pythonの標準ライブラリ,urllibを使ってクローリングする。
import sys
from urllib.request import urlopen

url = urlopen('http://sample.scraping-book.com/dp')

# HTTPヘッダーからエンコーディングを取得する
encoding = url.info().get_content_charset(failobj="utf-8")
print('encoding:', encoding, file=sys.stderr)

# エンコーディングを文字列にデコードして出力する
text = url.read().decode(encoding)
print(text)