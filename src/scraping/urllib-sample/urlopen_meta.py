import re
import sys
from urllib.request import urlopen

url = urlopen('http://sample.scraping-book.com/dp')

bytes_content = url.read()

# レスポンスボディの先頭1024バイトをASCII文字列としてデコードする
scanned_text = bytes_content[:1024].decode("ascii", errors="replace")

match = re.search(r'charset=["\']?([\w-]+)', scanned_text)
if match:
    encoding = match.group(1)
else:
    encoding = 'utf-8'

print('encoding:', encoding, file=sys.stderr)
text = bytes_content.decode(encoding)

print(text)